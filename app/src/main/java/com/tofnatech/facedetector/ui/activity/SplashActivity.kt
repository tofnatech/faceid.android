package com.tofnatech.facedetector.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tofnatech.facedetector.R
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Timer().schedule(object : TimerTask() {
            override fun run() {
                startActivity(
                    Intent(
                        this@SplashActivity,
                        CameraActivity::class.java
                    )
                )
            }
        }, 2000)
    }
}

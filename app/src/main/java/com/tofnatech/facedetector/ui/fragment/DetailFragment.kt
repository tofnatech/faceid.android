package com.tofnatech.facedetector.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.tofnatech.facedetector.databinding.FragmentDetailBinding
import com.tofnatech.facedetector.ui.activity.CameraActivity

class DetailFragment : Fragment() {

    private val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentDetailBinding.inflate(inflater, container, false).apply {
            faceResult = args.faceResult
            act = activity as CameraActivity
        }.root
    }
}

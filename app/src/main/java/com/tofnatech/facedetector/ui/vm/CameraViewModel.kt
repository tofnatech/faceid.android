package com.tofnatech.facedetector.ui.vm

import androidx.lifecycle.ViewModel
import com.tofnatech.facedetector.utils.CameraSurface
import javax.inject.Inject

/**
 * Created by burakakgun on 6.07.2019.
 */
class CameraViewModel @Inject constructor(val cameraSurface: CameraSurface) : ViewModel() {

    fun initCamera() = cameraSurface.initCamera()

    fun startPreview() = cameraSurface.startPreview()

    fun stopPreview() = cameraSurface.stopPreview()
}

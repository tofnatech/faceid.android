package com.tofnatech.facedetector.ui.activity

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.ImageFormat
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.tofnatech.facedetector.R
import com.tofnatech.facedetector.di.ViewModelInjectionField
import com.tofnatech.facedetector.di.qualifiers.ViewModelInjection
import com.tofnatech.facedetector.ui.vm.CameraViewModel
import com.tofnatech.facedetector.utils.CameraSurface
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_camera.*
import javax.inject.Inject

class CameraActivity : DaggerAppCompatActivity() {

    @Inject
    @ViewModelInjection
    lateinit var vm: ViewModelInjectionField<CameraViewModel>

    val navController by lazy {
        findNavController(R.id.nav_host_fragment)
    }

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_camera)
        checkPermission()
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.label) {
                resources.getString(R.string.fragment_camera_label) -> {
                    vm.get().startPreview()
                }
                resources.getString(R.string.fragment_detail_label) -> {
                    vm.get().stopPreview()
                }
            }
        }
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_DENIED
        ) ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CAMERA),
            CameraSurface.CAMERA_PERMISSION_REQUEST
        ) else vm.get().initCamera()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when {
            requestCode != CameraSurface.CAMERA_PERMISSION_REQUEST ||
                    grantResults[0] != PackageManager.PERMISSION_GRANTED -> return
            else -> vm.get().initCamera()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        surfaceView.holder.addCallback(vm.get().cameraSurface)
        surfaceView.holder.setFormat(ImageFormat.NV21)
    }

    override fun onResume() {
        super.onResume()
        vm.get().startPreview()
    }

    override fun onPause() {
        super.onPause()
        vm.get().stopPreview()
    }
}

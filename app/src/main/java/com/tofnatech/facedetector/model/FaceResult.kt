package com.tofnatech.facedetector.model

import android.graphics.PointF
import android.graphics.drawable.BitmapDrawable
import java.io.Serializable

class FaceResult : Any(), Serializable {

    private var midEye: PointF? = null
    private var eyeDist: Float = 0.toFloat()
    var confidence: Float = 0.toFloat()
    var pose: Float = 0.toFloat()
    var id: String
    var time: Long = 0
    var drawable: BitmapDrawable? = null

    init {
        id = ""
        midEye = PointF(0.0f, 0.0f)
        eyeDist = 0.0f
        confidence = 0.4f
        pose = 0.0f
        time = System.currentTimeMillis()
    }

    @Synchronized
    operator fun set(
        id: String,
        midEye: PointF,
        eyeDist: Float,
        confidence: Float,
        pose: Float,
        time: Long,
        drawable: BitmapDrawable
    ): FaceResult {
        this.id = id
        this.midEye!!.set(midEye)
        this.eyeDist = eyeDist
        this.confidence = confidence
        this.pose = pose
        this.time = time
        this.drawable = drawable
        return this
    }

    fun eyesDistance(): Float {
        return eyeDist
    }

    fun getMidPoint(pt: PointF) {
        pt.set(midEye)
    }
}

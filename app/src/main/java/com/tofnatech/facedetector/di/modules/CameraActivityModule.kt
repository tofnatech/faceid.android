package com.tofnatech.facedetector.di.modules

import android.app.Application
import com.microsoft.projectoxford.face.FaceServiceClient
import com.microsoft.projectoxford.face.FaceServiceRestClient
import com.tofnatech.facedetector.R
import com.tofnatech.facedetector.di.InjectionViewModelProvider
import com.tofnatech.facedetector.di.qualifiers.ViewModelInjection
import com.tofnatech.facedetector.ui.activity.CameraActivity
import com.tofnatech.facedetector.ui.vm.CameraViewModel
import dagger.Module
import dagger.Provides

/**
 * Created by burakakgun on 7.07.2019.
 */
@Module
class CameraActivityModule {

    @Provides
    @ViewModelInjection
    fun provideCameraActivityVM(
        activity: CameraActivity,
        viewModelProvider: InjectionViewModelProvider<CameraViewModel>
    ) = viewModelProvider.get(activity, CameraViewModel::class)

    @Provides
    fun provideFaceServiceClient(application: Application):
            FaceServiceClient = FaceServiceRestClient(
        application.getString(R.string.endpoint),
        application.getString(R.string.subscription_key)
    )
}

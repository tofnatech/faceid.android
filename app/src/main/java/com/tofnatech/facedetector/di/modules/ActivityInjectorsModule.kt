package com.tofnatech.facedetector.di.modules

import com.tofnatech.facedetector.ui.activity.CameraActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by burakakgun on 6.07.2019.
 */
@Module
abstract class ActivityInjectorsModule {

    @ContributesAndroidInjector(modules = [CameraActivityModule::class])
    abstract fun provideCameraActivity(): CameraActivity
}

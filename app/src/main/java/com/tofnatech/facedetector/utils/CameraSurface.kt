package com.tofnatech.facedetector.utils

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.hardware.Camera
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Handler
import android.util.Log
import android.view.SurfaceHolder
import android.view.WindowManager
import androidx.core.content.ContextCompat
import com.microsoft.projectoxford.face.FaceServiceClient
import com.tofnatech.facedetector.model.FaceResult
import com.tofnatech.facedetector.ui.activity.CameraActivity
import com.tofnatech.facedetector.ui.fragment.DetailFragmentDirections
import com.tofnatech.facedetector.utils.face_api.DetectionTask
import com.tofnatech.facedetector.utils.face_api.IdentificationTask
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.inject.Inject


/**
 * Created by burakakgun on 10.07.2019.
 */

@Suppress("DEPRECATION")
class CameraSurface @Inject constructor(
    val activity: CameraActivity,
    val faceServiceClient: FaceServiceClient
) :
    Camera.PreviewCallback, SurfaceHolder.Callback {
    private var mCamera: Camera? = null
    private var mDisplayRotation: Int = 0
    private var mDisplayOrientation: Int = 0
    private var previewWidth: Int = 0
    private var previewHeight: Int = 0
    private var isThreadWorking = false
    private var handler: Handler? = null
    private var detectThread: FaceDetectThread? = null
    private var prevSettingWidth: Int = 0
    private var prevSettingHeight: Int = 0
    private var fdet: android.media.FaceDetector? = null

    override fun onPreviewFrame(_data: ByteArray, _camera: Camera) {
        if (isThreadWorking.not()) {
            isThreadWorking = true
            waitForFdetThreadComplete()
            detectThread = handler?.let { FaceDetectThread(it) }
            detectThread!!.setData(_data)
            detectThread!!.start()
        }
    }

    fun initCamera() {
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        handler = Handler()
        surfaceStart()
        configureCamera(854, 480)
    }

    fun startPreview() {
        mCamera?.let {
            isThreadWorking = false
            it.startPreview()
            it.setPreviewCallback(this)
        }
    }

    fun stopPreview() = mCamera?.stopPreview()

    private fun surfaceStart() {
        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val cameraInfo = Camera.CameraInfo()
            mCamera = Camera.open(0)

            Camera.getCameraInfo(0, cameraInfo)
            mCamera?.setPreviewDisplay(activity.surfaceView.holder)
        }
    }

    private fun setDisplayOrientation() {
        mCamera?.let { cam ->
            mDisplayRotation = Util.getDisplayRotation(activity)
            mDisplayOrientation = Util.getDisplayOrientation(mDisplayRotation, 0)
            cam.setDisplayOrientation(mDisplayOrientation)
        }
    }

    private fun configureCamera(width: Int, height: Int) {
        stopPreview()
        mCamera?.let { cam ->
            val parameters = cam.parameters
            setOptimalPreviewSize(parameters, width, height)
            setAutoFocus(parameters)
            cam.parameters = parameters
        }
        setDisplayOrientation()
        val aspect = previewHeight.toFloat() / previewWidth.toFloat()
        fdet = android.media.FaceDetector(
            prevSettingWidth, (prevSettingWidth * aspect).toInt(),
            MAX_FACE
        )
        startPreview()
    }

    private fun setOptimalPreviewSize(
        cameraParameters: Camera.Parameters,
        width: Int,
        height: Int
    ) {
        val previewSizes = cameraParameters.supportedPreviewSizes
        val targetRatio = width.toFloat() / height
        val previewSize = Util.getOptimalPreviewSize(activity, previewSizes, targetRatio.toDouble())
        previewWidth = previewSize!!.width
        previewHeight = previewSize.height
        when {
            previewWidth / 4 > 360 -> {
                prevSettingWidth = 360
                prevSettingHeight = 270
            }
            previewWidth / 4 > 320 -> {
                prevSettingWidth = 320
                prevSettingHeight = 240
            }
            previewWidth / 4 > 240 -> {
                prevSettingWidth = 240
                prevSettingHeight = 160
            }
            else -> {
                prevSettingWidth = 160
                prevSettingHeight = 120
            }
        }
        cameraParameters.setPreviewSize(previewSize.width, previewSize.height)
    }

    private fun setAutoFocus(cameraParameters: Camera.Parameters) {
        val focusModes = cameraParameters.supportedFocusModes
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            cameraParameters.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
    }

    private fun waitForFdetThreadComplete() {
        if (detectThread != null && detectThread!!.isAlive) {
            try {
                detectThread!!.join()
                detectThread = null
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        surfaceStart()
    }

    override fun surfaceChanged(
        surfaceHolder: SurfaceHolder,
        format: Int,
        width: Int,
        height: Int
    ) {
        if (surfaceHolder.surface == null || mCamera == null) {
            return
        }
        configureCamera(width, height)
    }

    override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
        mCamera?.let { camera ->
            camera.setPreviewCallbackWithBuffer(null)
            camera.setErrorCallback(null)
            camera.release()
            mCamera = null
        }
    }

    private inner class FaceDetectThread(
        private val handler: Handler
    ) : Thread() {
        private var data: ByteArray? = null
        private var faceCropped: Bitmap? = null

        fun setData(data: ByteArray) {
            this.data = data
        }

        override fun run() {
            if (previewWidth > 0) {
                val aspect = previewHeight.toFloat() / previewWidth.toFloat()
                val w = prevSettingWidth
                val h = (prevSettingWidth * aspect).toInt()
                var bitmap: Bitmap? = Bitmap.createBitmap(
                    previewWidth, previewHeight, Bitmap.Config.RGB_565
                )
                val yuv = YuvImage(
                    data, ImageFormat.NV21,
                    bitmap!!.width, bitmap.height, null
                )
                val rectImage = Rect(0, 0, bitmap.width, bitmap.height)
                val baout = ByteArrayOutputStream()
                if (!yuv.compressToJpeg(rectImage, 100, baout)) {
                    Log.e("CreateBitmap", "compressToJpeg failed")
                }
                val bfo = BitmapFactory.Options()
                bfo.inPreferredConfig = Bitmap.Config.RGB_565
                bitmap = BitmapFactory.decodeStream(
                    ByteArrayInputStream(baout.toByteArray()), null, bfo
                )
                var bmp: Bitmap? = Bitmap.createScaledBitmap(bitmap!!, w, h, false)
                var xScale = previewWidth.toFloat() / prevSettingWidth.toFloat()
                var yScale = previewHeight.toFloat() / h.toFloat()
                val info = Camera.CameraInfo()
                Camera.getCameraInfo(0, info)
                var rotate = mDisplayOrientation
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT &&
                    mDisplayRotation % 180 == 0
                ) {
                    if (rotate + 180 > 360) {
                        rotate -= 180
                    } else
                        rotate += 180
                }

                when (rotate) {
                    90 -> {
                        bmp = ImageUtils.rotate(bmp, 90f)
                        xScale = previewHeight.toFloat() / bmp!!.width
                        yScale = previewWidth.toFloat() / bmp.height
                    }
                    180 -> bmp = ImageUtils.rotate(bmp, 180f)
                    270 -> {
                        bmp = ImageUtils.rotate(bmp, 270f)
                        xScale = previewHeight.toFloat() / h.toFloat()
                        yScale = previewWidth.toFloat() / prevSettingWidth.toFloat()
                    }
                }
                fdet = android.media.FaceDetector(bmp!!.width, bmp.height, MAX_FACE)
                val fullResults = arrayOfNulls<android.media.FaceDetector.Face>(MAX_FACE)
                fdet!!.findFaces(bmp, fullResults)
                fullResults.forEach {
                    it?.let {
                        val mid = PointF()
                        it.getMidPoint(mid)
                        mid.x *= xScale
                        mid.y *= yScale
                        val eyesDis = it.eyesDistance() * xScale
                        val confidence = it.confidence()
                        val pose = it.pose(android.media.FaceDetector.Face.EULER_Y)
                        val rect = Rect(
                            (mid.x - eyesDis * 1.20f).toInt(),
                            (mid.y - eyesDis * 0.55f).toInt(),
                            (mid.x + eyesDis * 1.20f).toInt(),
                            (mid.y + eyesDis * 1.85f).toInt()
                        )
                        if (rect.height() * rect.width() > 100 * 100) {
                            for (j in 0 until MAX_FACE) {
                                val faceResult = FaceResult().set(
                                    "burak",
                                    mid,
                                    eyesDis,
                                    confidence,
                                    pose,
                                    System.currentTimeMillis(),
                                    BitmapDrawable(activity.resources, bitmap)
                                )
                                faceCropped = ImageUtils.cropFace(
                                    faceResult, bitmap, rotate
                                )
                                checkFaceService(faceResult)
                            }
                        }
                    }
                }
                handler.post {
                    isThreadWorking = false
                }
            }
        }

        private fun checkFaceService(faceResult: FaceResult) {
            when {
                faceCropped != null -> handler.post {
                    if (activity.navController.currentDestination?.label !=
                        activity.resources.getString(
                            com.tofnatech.facedetector.R.string.fragment_detail_label
                        )
                    ) {
                        detect(faceResult)
                    }
                }
            }
        }

        private fun detect(faceResult: FaceResult) {
            val output = ByteArrayOutputStream()
            faceResult.drawable?.bitmap?.compress(
                Bitmap.CompressFormat.JPEG, 100, output
            )
            val inputStream = ByteArrayInputStream(output.toByteArray())
            val detectionTask =
                DetectionTask(faceServiceClient).execute(inputStream)
            val faces = detectionTask.get()
            if (faces.isNullOrEmpty().not()) {
                val identificationTask =
                    IdentificationTask(
                        activity.getString(com.tofnatech.facedetector.R.string.groupId),
                        faceServiceClient
                    ).execute(
                        faces[0].faceId
                    )
                val results = identificationTask.get()
                if (results.isNullOrEmpty().not()) {
                    if (results[0].candidates.isNullOrEmpty().not()) {
                        val personId = results[0].candidates[0].personId
                        when (personId.toString()) {
                            activity.getString(com.tofnatech.facedetector.R.string.burak_face_id) ->
                                faceResult.id = "Burak Akgün"
                            activity.getString(com.tofnatech.facedetector.R.string.altug_face_id) ->
                                faceResult.id = "Altuğ Karayel"
                            activity.getString(com.tofnatech.facedetector.R.string.tenzile_face_id) ->
                                faceResult.id = "Tenzile"
                        }
                        ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                            .startTone(ToneGenerator.TONE_SUP_RINGTONE, 300)
                        activity.navController.navigate(
                            DetailFragmentDirections.actionCameraActivityToDetailFragment(
                                faceResult
                            )
                        )
                    } else {
                        startPreview()
                    }
                } else {
                    startPreview()
                }
            } else {
                startPreview()
            }
        }
    }

    companion object {
        private const val MAX_FACE = 1
        const val CAMERA_PERMISSION_REQUEST = 1453
    }
}

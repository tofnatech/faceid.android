package com.tofnatech.facedetector.utils.face_api

import android.os.AsyncTask
import com.microsoft.projectoxford.face.FaceServiceClient
import com.microsoft.projectoxford.face.contract.IdentifyResult
import java.util.*

/**
 * Created by burakakgun on 13.07.2019.
 */
class IdentificationTask constructor(
    private var mPersonGroupId: String,
    private var faceServiceClient: FaceServiceClient
) : AsyncTask<UUID, String, Array<IdentifyResult>>() {

    override fun doInBackground(vararg params: UUID): Array<IdentifyResult>? {
        return try {
            publishProgress("Identifying...")
            faceServiceClient.identityInLargePersonGroup(
                this.mPersonGroupId,
                params,
                1
            )
        } catch (e: Exception) {
            publishProgress(e.message)
            null
        }
    }

    override fun onPreExecute() = Unit

    override fun onProgressUpdate(vararg values: String) = Unit

    override fun onPostExecute(result: Array<IdentifyResult>) = Unit
}

package com.tofnatech.facedetector.utils.face_api

import android.os.AsyncTask
import com.microsoft.projectoxford.face.FaceServiceClient
import com.microsoft.projectoxford.face.contract.Face
import java.io.InputStream

/**
 * Created by burakakgun on 13.07.2019.
 */
class DetectionTask(private val faceServiceClient: FaceServiceClient) :
    AsyncTask<InputStream, String, Array<Face>>() {

    override fun doInBackground(vararg params: InputStream): Array<Face>? {
        return try {
            publishProgress("Detecting...")
            faceServiceClient.detect(
                params[0],
                true,
                false, null
            )
        } catch (e: Exception) {
            publishProgress(e.message)
            null
        }
    }

    override fun onPreExecute() = Unit

    override fun onProgressUpdate(vararg values: String) = Unit

    override fun onPostExecute(result: Array<Face>?) = Unit
}

package com.tofnatech.facedetector.utils

import android.app.Activity
import android.graphics.Point
import android.hardware.Camera
import android.hardware.Camera.Size
import android.util.Log
import android.view.Surface
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.tofnatech.facedetector.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs
import kotlin.math.min

@BindingAdapter("app:srcValue")
fun setSrcResource(view: ImageView, value: String) {
    when {
        value.contains("burak", true) -> view.setBackgroundResource(R.drawable.burak)
        value.contains("altuğ", true) -> view.setBackgroundResource(R.drawable.altug)
        value.contains("tenzile", true) -> view.setBackgroundResource(R.drawable.tenzile)
    }
}

@BindingAdapter("app:dateStr")
fun setDateString(view: TextView, value: Long) {
    view.text = SimpleDateFormat("dd.MM.yyyy, hh:mm").format(Date(value))
}

const val tolerance = 0.001
const val degree = 360

@Suppress("DEPRECATION")
object Util {
    private const val TAG = "Util"
    fun getDisplayRotation(activity: Activity): Int {
        val rotation = activity.windowManager.defaultDisplay
            .rotation
        when (rotation) {
            Surface.ROTATION_0 -> return 0
            Surface.ROTATION_90 -> return 90
            Surface.ROTATION_180 -> return 180
            Surface.ROTATION_270 -> return 270
        }
        return 0
    }

    fun getDisplayOrientation(degrees: Int, cameraId: Int): Int {
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)
        var result: Int
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % degree
            result = (degree - result) % degree
        } else {
            result = (info.orientation - degrees + degree) % degree
        }
        return result
    }

    fun getOptimalPreviewSize(
        currentActivity: Activity,
        sizes: List<Size>?,
        targetRatio: Double
    ): Size? {
        if (sizes == null) return null
        var optimalSize: Size? = null
        var minDiff = java.lang.Double.MAX_VALUE
        val point = getDefaultDisplaySize(currentActivity, Point())
        val targetHeight = min(point.x, point.y)
        for (size in sizes) {
            val ratio = size.width.toDouble() / size.height
            if (abs(ratio - targetRatio) > tolerance) continue
            if (abs(size.height - targetHeight) < minDiff) {
                optimalSize = size
                minDiff = Math.abs(size.height - targetHeight).toDouble()
            }
        }
        if (optimalSize == null) {
            Log.w(TAG, "No preview size match the aspect ratio")
            minDiff = java.lang.Double.MAX_VALUE
            for (size in sizes) {
                if (abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size
                    minDiff = Math.abs(size.height - targetHeight).toDouble()
                }
            }
        }
        return optimalSize
    }

    private fun getDefaultDisplaySize(activity: Activity, size: Point): Point {
        val d = activity.windowManager.defaultDisplay
        d.getSize(size)
        return size
    }
}

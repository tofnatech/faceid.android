/**
 * Find which updates are available by running
 *     `$ ./gradlew buildSrcVersions`
 * This will only update the comments.
 *
 * YOU are responsible for updating manually the dependency version.
 */
object Versions {
    const val appcompat: String = "1.0.2" 

    const val constraintlayout: String = "1.1.3" 

    const val core_ktx: String = "1.0.2" 

    const val androidx_databinding: String = "3.4.2" 

    const val legacy_support_v4: String = "1.0.0" 

    const val lifecycle_extensions: String = "2.0.0" 

    const val androidx_navigation: String = "2.1.0-alpha06" 

    const val com_android_tools_build_gradle: String = "3.4.2" 

    const val lint_gradle: String = "26.4.2" 

    const val com_google_dagger: String = "2.23.2" 

    const val face: String = "1.4.4" 

    const val gradle_code_quality_tools_plugin: String = "0.18.0" 

    const val de_fayard_buildsrcversions_gradle_plugin: String = "0.3.2" 

    const val circleimageview: String = "3.0.0"

    const val org_jetbrains_kotlin: String = "1.3.41" 

    const val sonarqube_gradle_plugin: String = "2.7.1" 

    /**
     *
     *   To update Gradle, edit the wrapper file at path:
     *      ./gradle/wrapper/gradle-wrapper.properties
     */
    object Gradle {
        const val runningVersion: String = "5.5"

        const val currentVersion: String = "5.5.1"

        const val nightlyVersion: String = "5.6-20190713000027+0000"

        const val releaseCandidate: String = ""
    }
}
